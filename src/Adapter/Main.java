package Adapter;

import static java.lang.System.out;
public class Main {

	public static void main(String[] args) {
		WeatherService service = new RussianWeather();
		service.setPosition("Москва");

		System.out.println("Москва");
		System.out.printf("Температура (C)          : %4.1f%n",
				service.getTemperature());
		System.out.printf("Скорость ветра (м/с)     : %4.1f%n",
				service.getWind());
		System.out.printf("Ощущаемая температура (C): %4.1f%n",
				service.getFeelsLikeTemperature());
		
		//не сработает - не совместимые интерфейсы
		//service = new USWeatherService();

		// используем адаптер
		service = new USWeatherAdapter();
		service.setPosition("Нью-Йорк");

		System.out.println("Нью-Йорк");
		out.printf("Температура (C)          : %4.1f%n",
				service.getTemperature());
		out.printf("Скорость ветра (м/с)     : %4.1f%n",
				service.getWind());
		out.printf("Ощущаемая температура (C): %4.1f%n",
				service.getFeelsLikeTemperature());
	}

}
