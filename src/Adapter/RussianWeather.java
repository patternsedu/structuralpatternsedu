package Adapter;

public class RussianWeather implements WeatherService {

	String city;
	
	/**
	 * Возвращает температуру.
	 * @return температура в градусах Цельсия
	 */
	@Override
	public double getTemperature() {
		return switch (city) {
			case "Москва" -> 25;
			case "Санкт-Петербург" -> 18;
			default -> 20;
		};
	}

	/**
	 * Возвращает скорость ветра.
	 * @return скорость ветра в м/с
	 */
	@Override
	public double getWind() {
		return switch (city) {
			case "Москва" -> 5;
			case "Санкт-Петербург" -> 13;
			default -> 1;
		};
	}

	/**
	 * Возвращает ощущаемую температуру.
	 * @return температура в градусах Цельсия
	 */
	@Override
	public double getFeelsLikeTemperature() {
		return switch (city) {
			case "Москва" -> 23;
			case "Санкт-Петербург" -> 16;
			default -> 20;
		};
	}

	@Override
	public void setPosition(String city) {
		this.city = city;
	}

}
