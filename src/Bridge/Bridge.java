package Bridge;

import Bridge.platforms.Database;
import Bridge.ui.UserInterface;

public class Bridge {
    private Database db;
    private UserInterface ui;

    public Bridge(Database db, UserInterface ui) {
        this.db = db;
        this.ui = ui;
    }

    // ...
}
