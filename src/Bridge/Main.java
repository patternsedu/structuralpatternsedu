package Bridge;

import Bridge.platforms.Database;
import Bridge.platforms.MicrosoftSQLDB;
import Bridge.platforms.MySQLDB;
import Bridge.ui.AdminInterface;
import Bridge.ui.UserInterface;

public class Main {

	/**
	 * Создать программу, работающую с базой данных.
	 * Программа должна уметь работать с двумя (или более) видами баз данных (слой реализации платформы)
	 * и иметь два (или более) пользовательских интерфейсов (административный и пользовательский),
	 * отображающих информацию из базы данных.
	 */
	public static void main(String[] args) {
		{
			Database db = new MicrosoftSQLDB();
			UserInterface ui = new UserInterface(db);
			
			ui.login("Sergey");
			ui.drawInterface();
		}
		{
			Database db = new MicrosoftSQLDB();
			UserInterface ui = new AdminInterface(db);
			
			ui.login("Sergey");
			ui.drawInterface();
		}
		{
			Database db = new MySQLDB();
			UserInterface ui = new UserInterface(db);
			
			ui.login("Sergey");
			ui.drawInterface();
		}
		{
			Database db = new MySQLDB();
			UserInterface ui = new AdminInterface(db);
			
			ui.login("Sergey");
			ui.drawInterface();
		}

	}

}
