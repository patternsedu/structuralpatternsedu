package Composite;

import Composite.widgets.*;

public class Main {

	public static void main(String[] args) {
		// можно использовать Builder для создания дерева объектов
		CompositeControl mainWin = new MainWindow();
		CompositeControl frame1 = new CompositeControl();
		CompositeControl frame2 = new CompositeControl();
		CompositeControl frame3 = new CompositeControl();
		frame1.add(new Label("Login")).add(new Button("OK"));
		frame2.add(new Label("Password")).add(new Button("Verify"));
		frame3.add(new Button("Print"));
		mainWin.add(frame1).add(frame2).add(frame3);

		// отрисовка окна
		mainWin.draw();
	}
}
