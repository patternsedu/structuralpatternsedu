package Decorator;

import Composite.widgets.*;
import Decorator.decorators.CompositeTitleDecorator;

public class Main {

	public static void main(String[] args) {
		
		// декорируем главное окно, превращая его в оно с заголовком
		CompositeControl mainWin = new CompositeTitleDecorator(
				new MainWindow(), "Main Window title");
		
		CompositeControl frame1 = new CompositeControl()
				.add(new Label("Login")).add(new Button("OK"));
		CompositeControl frame2 = new CompositeControl()
				.add(new Label("Password")).add(new Button("Verify"));
		// декорируем контейнер с кнопкой Print
		CompositeControl frame3 = new CompositeTitleDecorator(
				new CompositeControl(), "CMD")
				.add(new Button("Print"));

		mainWin.add(frame1).add(frame2).add(frame3);
		
		// отрисовка окна
		mainWin.draw();
	}
}
