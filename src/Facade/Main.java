package Facade;

import Facade.facades.AppCreatorFacade;
import Facade.facades.SimpleAppCreatorFacade;

public class Main {

	public static void main(String[] args) {
		
		AppCreatorFacade facade = new SimpleAppCreatorFacade();
		facade.showApp("Login", "@OK", "Password", "@Verify");

	}

}
