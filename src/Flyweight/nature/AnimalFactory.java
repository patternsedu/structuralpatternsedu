package Flyweight.nature;

import java.util.Random;

public class AnimalFactory {
	protected Random r = new Random();
	public Animal createButterfly() {
		return new Animal(r.nextInt(), r.nextInt(), "./images/butterfly.png");
	}
	public Animal createLadybug() {
		return new Animal(r.nextInt(), r.nextInt(),"./images/ladybug.png");
	}
	public Animal createSnail() {
		return new Animal(r.nextInt(), r.nextInt(),"./images/snail.png");
	}
}
