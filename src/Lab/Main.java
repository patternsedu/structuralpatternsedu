package Lab;

import Lab.graph.Line;
import Lab.graph.Point;
import Lab.graph.TriangleAdapter;
import Lab.scene.Scene;
import Lab.scene.SceneCreatorFacade;
import Lab.scene.composite.FigureControl;
import Lab.scene.decorator.FigureTitleDecorator;

public class Main {
    public static void main(String[] args) {

        // Task 1
//        Figure triangle = new Triangle(
//                new Coord(10D, 20D),
//                new Coord(20D, 40D),
//                new Coord(40D, 60D)
//        );
//        System.out.println(triangle.print());

        // Task 1-3
//        FigureControl figureSimple = new FigureTitleDecorator("Simple Figure", new FigureControl()
//                .add(new Point(10, 20, "red"))
//                .add(new Line(
//                        new Point(10, 20, "red"),
//                        new Point(20, 40, "red"),
//                        "black"
//                ))
//        );
//
//        FigureControl figureComplex = new FigureTitleDecorator("Complex Figure", new FigureControl()
//                .add(new Point(10, 20, "red"))
//                .add(new Point(10, 20, "red"))
//                .add(new TriangleAdapter(
//                        new Point(10, 20, "red"),
//                        new Point(20, 40, "red"),
//                        new Point(40, 60, "red"),
//                        "black"
//                ))
//        );
//
//        Scene.getInstance().getMainFrame().add(figureSimple).add(figureComplex);
//        System.out.println(scene);

        // Task 4
        SceneCreatorFacade sceneCreator = new SceneCreatorFacade();
        sceneCreator.addFigures(
                "@SimpleFigure", "Point", "Line",
                "@ComplexFigure", "Point", "Point", "Triangle"
        );
        System.out.println(Scene.getInstance());
    }
}
