package Lab.external;

public class Coord {
    /**
     * Координата X.
     */
    private Double x;
    /**
     * Координата Y.
     */
    private Double y;

    public Coord(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }
}
