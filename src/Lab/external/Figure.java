package Lab.external;

import java.util.stream.Stream;

/**
 * Базовый класс замкнутой фигуры.
 */
public abstract class Figure {
    protected String name;
    protected Coord[] points;

    protected Figure(String name, Coord[] points) {
        this.name = name;
        this.points = points;
    }

    /**
     * Метод для распечатки фигуры.
     */
    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s%n", name));
        Stream.of(points).forEach(point -> sb.append(String.format("# point(%s, %s)%n", point.getX(), point.getY())));
        return sb.toString();
    }
}
