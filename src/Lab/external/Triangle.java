package Lab.external;

public class Triangle extends Figure {
    public Triangle(Coord point1, Coord point2, Coord point3) {
        super("Triangle", new Coord[]{point1, point2, point3});
    }

    public Coord getPoint1() {
        return super.points[0];
    }

    public void setPoint1(Coord point1) {
        super.points[0] = point1;
    }

    public Coord getPoint2() {
        return super.points[1];
    }

    public void setPoint2(Coord point2) {
        super.points[1] = point2;
    }

    public Coord getPoint3() {
        return super.points[2];
    }

    public void setPoint3(Coord point3) {
        super.points[2] = point3;
    }
}
