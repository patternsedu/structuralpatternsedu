package Lab.graph;

public class Circle extends GraphObject {
	/**
     * Центральная точка окружности.
	 */
	private Point center;

	/**
	 * Радиус окружности.
	 */
	private int radius;

	public Circle(Point center, int radius, String color) {
		super(color);
		this.center = center;
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return String.format("Circle [%s, %d] %s", center, radius, color);
	}

	public Point getCenter() {
		return center;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
}
