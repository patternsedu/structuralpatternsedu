package Lab.graph;

public abstract class GraphObject {
    /**
     * Цвет графического объекта.
     */
    protected String color;

    protected GraphObject(String color) {
        this.color = color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
