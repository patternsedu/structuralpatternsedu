package Lab.graph;

public class Line extends GraphObject {
	/**
     * Первая точка линии.
	 */
	private Point point1;

	/**
	 * Вторая точка линии.
	 */
	private Point point2;

	public Line(Point point1, Point point2, String color) {
		super(color);
		this.point1 = point1;
		this.point2 = point2;
	}

	@Override
	public String toString() {
		return String.format("Line [%s, %s] %s", point1, point2, color);
	}

	public Point getPoint1() {
		return point1;
	}

	public void setPoint1(Point point1) {
		this.point1 = point1;
	}

	public Point getPoint2() {
		return point2;
	}

	public void setPoint2(Point point2) {
		this.point2 = point2;
	}
}
