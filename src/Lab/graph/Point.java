package Lab.graph;

public class Point extends GraphObject {
	/**
	 * Координата X.
	 */
	private int x;

	/**
	 * Координата Y.
	 */
	private int y;
	
	public Point(int x, int y, String color) {
		super(color);
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return String.format("Point (%d, %d) %s", x, y, color);
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getX() {
		return x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return y;
	}
}
