package Lab.graph;

import Lab.external.Coord;
import Lab.external.Triangle;

public class TriangleAdapter extends GraphObject {
    private final Triangle figure;
    private Point point1;
    private Point point2;
    private Point point3;

    public TriangleAdapter(Point point1, Point point2, Point point3, String color) {
        super(color);
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.figure = new Triangle(
                new Coord((double) point1.getX(), (double) point1.getY()),
                new Coord((double) point2.getX(), (double) point2.getY()),
                new Coord((double) point3.getX(), (double) point3.getY())
        );
    }

    @Override
    public String toString() {
        return figure.print()
                + String.format("> colors: (%s, %s, %s), %s",
                        point1.getColor(), point2.getColor(), point3.getColor(), color
                );
    }

    public Point getPoint1() {
        return point1;
    }

    public void setPoint1(Point point1) {
        this.point1 = point1;
        this.figure.setPoint1(new Coord((double) point1.getX(), (double) point1.getY()));
    }

    public Point getPoint2() {
        return this.point2;
    }

    public void setPoint2(Point point2) {
        this.point2 = point2;
        this.figure.setPoint2(new Coord((double) point2.getX(), (double) point2.getY()));
    }

    public Point getPoint3() {
        return point3;
    }

    public void setPoint3(Point point3) {
        this.point3 = point3;
        this.figure.setPoint3(new Coord((double) point3.getX(), (double) point3.getY()));
    }
}
