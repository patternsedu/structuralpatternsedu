package Lab.scene;

import Lab.scene.composite.FrameControl;

/**
 * Singleton сцена.
 */
public class Scene {
    /**
     * Данные сцены.
     */
    private final FrameControl mainFrame = new FrameControl();

    public FrameControl getMainFrame() {
        return mainFrame;
    }

    @Override
    public String toString() {
        return mainFrame.print();
    }

    private static Scene instance;

    public static synchronized Scene getInstance() {
        if (instance == null)
            instance = new Scene();

        return instance;
    }
}
