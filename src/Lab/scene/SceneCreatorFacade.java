package Lab.scene;

import Lab.graph.Circle;
import Lab.graph.GraphObject;
import Lab.graph.Line;
import Lab.graph.Point;
import Lab.graph.TriangleAdapter;
import Lab.scene.composite.FigureControl;
import Lab.scene.composite.FrameControl;
import Lab.scene.decorator.FigureTitleDecorator;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class SceneCreatorFacade {
    private final Random random = new Random();

    private static final int RANDOM_PARAM_MIN = 0;
    private static final int RANDOM_PARAM_MAX = 100;

    private static final List<String> COLORS = List.of("red", "blue", "pink", "yellow");

    /**
     * Получить произвольное целочисленное значение.
     */
    private int getRandomNumber(int min, int max) {
        return random.nextInt(max - min) + min;
    }

    /**
     * Получить произвольное значение для параметра графического объекта.
     */
    private int getRandomParam() {
        return getRandomNumber(RANDOM_PARAM_MIN, RANDOM_PARAM_MAX);
    }

    /**
     * Получить произвольный цвет графического объекта.
     */
    private String getRandomColor() {
        return COLORS.get(getRandomNumber(0, COLORS.size()));
    }

    /**
     * Инициализировать фигуру.
     */
    private FigureControl initFigure(String param) {
        String title = param.substring(1);
        return new FigureTitleDecorator(title, new FigureControl());
    }

    /**
     * Инициализировать графический объект.
     */
    private GraphObject initGraphObject(String param) {
        return switch (param) {
            case "Point" -> new Point(
                    getRandomParam(), getRandomParam(), getRandomColor()
            );
            case "Line" -> new Line(
                    new Point(getRandomParam(), getRandomParam(), getRandomColor()),
                    new Point(getRandomParam(), getRandomParam(), getRandomColor()),
                    getRandomColor()
            );
            case "Circle" -> new Circle(
                    new Point(getRandomParam(), getRandomParam(), getRandomColor()),
                    getRandomParam(),
                    getRandomColor()
            );
            case "Triangle" -> new TriangleAdapter(
                    new Point(getRandomParam(), getRandomParam(), getRandomColor()),
                    new Point(getRandomParam(), getRandomParam(), getRandomColor()),
                    new Point(getRandomParam(), getRandomParam(), getRandomColor()),
                    getRandomColor()
            );
            default -> null;
        };
    }

    /**
     * Добавление фигур на сцену с помощью команды.
     * @param params параметры команды
     */
    public void addFigures(String... params) {
        FrameControl mainFrame = Scene.getInstance().getMainFrame();
        FigureControl currentFigure = null;
        for(String param : params) {
            if (param.startsWith("@")) {
                if (!Objects.isNull(currentFigure))
                    mainFrame.add(currentFigure);
                currentFigure = initFigure(param);
            } else {
                if (Objects.isNull(currentFigure))
                    continue;
                Optional.ofNullable(initGraphObject(param))
                        .ifPresentOrElse(
                                currentFigure::add,
                                () -> System.out.printf("[WARNING] Not correct param: %s", param)
                        );
            }
        }
        if (!Objects.isNull(currentFigure))
            mainFrame.add(currentFigure);
    }
}
