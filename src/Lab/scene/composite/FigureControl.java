package Lab.scene.composite;

import Lab.graph.GraphObject;

import java.util.ArrayList;
import java.util.List;

public class FigureControl implements SceneComponent {
	private static final String FIGURE_BORDER = "************************";

	/**
	 * Содержимое фигуры.
	 */
	protected List<GraphObject> data = new ArrayList<>();

	/**
	 * Очистка содержимого фигуры.
	 */
	public void reset() {
		data.clear();
	}

	/**
	 * Добавление графического объекта в содержимое фигуры.
	 */
	public FigureControl add(GraphObject graphObject) {
		data.add(graphObject);
		return this;
	}

	/**
	 * Получение всего содержимого фигуры.
	 */
	public List<GraphObject> getData() {
		return data;
	}

	@Override
	public String print() {
		StringBuilder sb = new StringBuilder();
		sb.append(FIGURE_BORDER + '\n');
		data.forEach(graphObject -> sb.append(String.format("-> %s%n", graphObject)));
		sb.append(FIGURE_BORDER+ '\n');
		return sb.toString();
	}
}
