package Lab.scene.composite;

import java.util.ArrayList;
import java.util.List;

public class FrameControl implements SceneComponent {
	protected static final String FRAME_BORDER = "++++++++++++++++++++++++";

	/**
	 * Дочерние компоненты фрейма.
	 */
	private final List<SceneComponent> children = new ArrayList<>();

	/**
	 * Добавление дочернего компонента.
	 */
	public FrameControl add(SceneComponent c) {
		children.add(c);
		return this;
	}

	/**
	 * Удаление дочернего компонента.
	 */
	public void remove(SceneComponent c) {
		children.remove(c);
	}

	/**
	 * Список дочерних компонентов.
	 */
	public List<SceneComponent> getChildren() {
		return children;
	}

	@Override
	public String print() {
		StringBuilder sb = new StringBuilder();
		sb.append(FRAME_BORDER + '\n');
		children.forEach(child -> sb.append(child.print()));
		sb.append(FRAME_BORDER + '\n');
		return sb.toString();
	}
}
