package Lab.scene.composite;

public interface SceneComponent {
	/**
	 * Метод для печати компонента сцены.
	 */
	String print();
}
