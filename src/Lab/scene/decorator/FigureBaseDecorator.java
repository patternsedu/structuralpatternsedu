package Lab.scene.decorator;

import Lab.scene.composite.FigureControl;

/**
 * Базовый декоратор для графических фигур.
 */
public abstract class FigureBaseDecorator extends FigureControl {
    protected FigureControl wrappee;

    protected FigureBaseDecorator(FigureControl wrappee) {
        this.wrappee = wrappee;
        this.data = this.wrappee.getData();
    }
}
