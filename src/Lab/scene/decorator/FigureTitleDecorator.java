package Lab.scene.decorator;

import Lab.scene.composite.FigureControl;

/**
 * Декоратор добавления заголовка для графических фигур.
 */
public class FigureTitleDecorator extends FigureBaseDecorator {
    private final String title;

    public FigureTitleDecorator(String title, FigureControl wrappee) {
        super(wrappee);
        this.title = title;
    }

    @Override
    public String print() {
        return String.format("%n%s%n%s", title, wrappee.print());
    }
}
