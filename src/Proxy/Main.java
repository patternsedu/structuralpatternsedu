package Proxy;

public class Main {

	public static void main(String[] args) {
		// не будет работать, если сервис удаленный, или требует доп. действия
		//WeatherService service = new RemoteRussianWeatherService();
		
		// поэтому используем прокси
		try (RussianWeatherProxy service = new RussianWeatherProxy()) {

			service.setPosition("Москва");

			System.out.println("Москва");
			System.out.printf("Температура (C)          : %4.1f%n",
					service.getTemperature());
			System.out.printf("Скорость ветра (м/с)     : %4.1f%n",
					service.getWind());
			System.out.printf("Ощущаемая температура (C): %4.1f%n",
					service.getFeelsLikeTemperature());
		}
	}
}
